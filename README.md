# Reverse Proxy Basics

![Reverse Proxy Bild](images/reverse-proxy.webp)

## Einleitung

Dieses Repository soll dabei helfen zu verstehen, was die Funktionsweise eines Reverse Proxies ist und welche Anforderungen ein solcher abdecken kann.

Grundsätzlich wenn wir von Proxies sprechen, meinen wir ein Dienst der genutzt werden kann um auf andere Systeme zuzugreifen. In diesem Sinne ist ein Proxy ein "Mittelsmann". Es gibt viele verschiedene Arten von Proxies (je nachdem welches Protokoll eingesetzt wird). Für relevant ist hier jetzt aber der Web Proxy (also für das Protokoll http).

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

### Wichtige Begriffe

- Backend Server -> Server welcher die effektive Web Applikation hosted. Kann z.B. ein Python Flask Server sein oder ein normaler Webserver.
- Upstream -> Verbindung zwischen Reverse Proxy und Backend Server
- Downstream -> Verbindung zwischen Client und Reverse Proxy
- Request -> Anfrage eines Clients
- HTTP Header -> Das sind Felder in einem HTTP Request oder Response welche Zusatzinformationen zur Verfügung stellen.

## Funktionsweise eines Reverse Proxies

Ein Reverse Web Proxy dient erfüllt folgende Aufgaben:

- **Load Balancing** - Über den Reverse Proxy können Anfragen auf mehrere Backend Server verteilt werden. Ein Reverse Proxy kann mit Hilfe von Health Checks prüfen welche Upstream Server zur Verfügung stehen oder nicht.
- **Security** - Clients verbinden nicht direkt auf die Backend Server, sondern "sehen" nur den Reverse Proxy. Zudem kann auf dem Reverse Proxy die Daten welche der CLient schickt geprüft und gegebenenfalls angepasst werden, bevor sie Upstream weitergeleitet wird. Ein Reverse Proxy kann auch TLS Verbindungen terminieren.
- **Performance** - Ein Reverse Proxy kann Inhalte Cache oder statische Dateien (wie Bilder, CSS und Javascript) selber zur Verfügung stellen. Das reduziert die Last der Upstream Server.
- **Wartung** - Wenn ein Teil der Upstream Server nicht verfügbar ist (wegen Wartung o.ä.) dann kann der Reverse Proxy selbständig Fehlerseiten an den Client schicken oder den Request auf andere Upstream Server weiterleiten.

## Wichtige Aspekte im Einsatz eines Reverse Proxies

Wie oben erwähnt, steht ein Reverse Proxy als Mittelsperson zwischen Clients und Applikations-/Webserver. Dadurch besteht keine direkte Verbindung zwischen Client und den eigentlichen Servern welche die App hosten.

Das bedeutet die Upstream Server haben keinen direkten Zugriff auf die IP Adresse der Clients, was je nachdem wichtig ist (z.B. um korrekte Log Einträge schreiben zu können oder IP basierte Authorisierung zu ermöglichen). Damit die Upstream Server die Client IP Adresse trotzdem kennen, muss der Reverse Proxy bestimmte HTTP Header setzen. Diese Header werden allgemein als X-Forwarded-* Headers.

## Struktur dieses Repos

Dieses Repository hat mehrere Unterordner und pro Unterordner gibt es ein Beispiel einer Reverse Proxy Funktion und jeweils ein kleines Readme dazu.

- [ ] [Basic Reverse Proxy](/basic/README.md)
- [ ] [Multiple Upstream Servers](/loadbalancing/README.md)
- [ ] [Static Files](/static-files/README.md)
- [ ] [Security](/security/README.md) - NOT DONE
- [ ] [Custom Error Pages](/custom-error/README.md) - Zum selber machen

## Verwendete Software und notwendige Tools

Dieses Projekt nutzt folgende Software

- Caddy (als Reverse Proxy)
- nginx (als Upstream Applikationsserver)
- docker
- docker-compose
- dsa Zertifikat wurde mit folgendem Befehl erstellt: ```openssl req -newkey rsa:2048 -nodes -keyout reverseproxy/config/ssl/hello.demo.ch.key -x509 -days 3600 -out reverseproxy/config/ssl/hello.demo.ch.crt```

Idealerweise sind folgende Tools installiert um die einzelnen Übungen machen zu können.

- docker
- docker-compose

## License

This repo and its content is provided Creative Commons Zero License and can be freely used.
