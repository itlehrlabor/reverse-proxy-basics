# Reverse Proxy -  Load Balancing Beispiel

![Reverse Proxy Bild](../images/Load Balancing.png)

Dieses Beispiel beinhaltet eine Umgebung mit Reverse Proxy und zwei Upstream Web Server die eine Webseite zur Verfügung stellen. Die Webseiten sind leicht unterschiedlich (Webserver 1 und 2 im Titel).

Die Webseite kann über den Hostnamen test.demo.ch zugegriffen werden (bitte im /etc/hosts File des Hosts einen entsprechenden Eintrag für 127.0.0.1 machen).

Die Umgebung kannst du mit ```docker-compose up``` starten.

Schau dir die Files an um herauszufinden wie das funktioniert.

## Auf die App zugreifen

Im Browser <http://test.demo.ch/> eingeben

## Übung

- [ ] Versuche in den Log Files des herauszufinden welcher Server den Request abarbeitet.
- [ ] Versuche auf Basis von HTTP Headern (in deinem Browser) herauszufinden welcher Backend Server den Request handelt.
- [ ] Stoppe einmal einen Appserver, mit dem Befehl ```docker-compose stop appserver01``` und schau was der Caddy Reverse Proxy im Log meldet und wie das ganze auf deinem Client aussieht.
- [ ] Versuche zu verstehen wie die Caddy Konfiguration funktioniert
