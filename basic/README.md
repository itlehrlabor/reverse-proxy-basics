# Reverse Proxy -  Basic Beispiel

![Reverse Proxy Bild](/images/basic.png)

Dieses Beispiel beinhaltet eine einfache Umgebung mit Reverse Proxy und Upstream Server der eine Webseite zur Verfügung stellt.

Die Webseite kann über den Hostnamen test.demo.ch zugegriffen werden (bitte im /etc/hosts File des Hosts einen entsprechenden Eintrag für 127.0.0.1 machen).

Die Umgebung kannst du mit ```docker-compose up``` starten.

Schau dir die Files an um herauszufinden wie das funktioniert.

## Auf die App zugreifen

Im Browser <http://test.demo.ch/> eingeben

## Übung

Versuche in den Log Files des Webserver Containers zu schauen ob du die IP Adresse des Clients herausfindest.